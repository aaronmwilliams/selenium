# Selenium Readme

### Purpose of project
To demonstrate a simple Selenium framework.

### Installing and Running

##### Checking Out Project

Import as a **GRADLE** project.

##### Running Tests
After the project has built you can run the tests using the Gradle wrapper commands.

```
./gradlew clean build
```
or
```
./gradlew test
```

To run the tests you will need to use the following VM options. Set these as your default jUnit & grade run configurations.
```
-ea
-Dbrowser=chrome
-Dwebdriver.chrome.driver=[THE PATH TO YOUR CHROMEDRIVER.EXE]
```