package infrastructure;

import pageobjects.GooglePage;

public class PageNavigator {

    public GooglePage goToGoogle() {
        return new GooglePage();
    }
}
