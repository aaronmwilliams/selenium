package infrastructure;

import com.google.common.base.Function;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.util.concurrent.TimeUnit.SECONDS;

public abstract class TestUtils {

    protected WebDriver driver = new ChromeDriver();
    private By waitForBy;

    protected WebElement waitForElement(By by) {
        waitForBy = by;
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(10, SECONDS)
                .pollingEvery(1, SECONDS)
                .ignoring(NoSuchElementException.class);

        WebElement foundElement = wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                return driver.findElement(waitForBy);
            }
        });
        return foundElement;
    }

    public void takeScreenshot(String pageAndStep) {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyHHMMss");
        WebDriver augmentedDriver = new Augmenter().augment(driver);
        File screenshot = ((TakesScreenshot) augmentedDriver).
                getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(screenshot, new File("src\\test\\resources\\"
                    + pageAndStep
                    + sdf.format(date)
                    + " Screenshot.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
