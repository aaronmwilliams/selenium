package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.stream.Collectors;

public class GooglePage extends AbstractPage {

    public GooglePage() {
        super();
    }

    @Override
    protected void buildPage() {
        expectedPageTitle = "Google";
        url = "http://www.google.com/";
    }

    public GooglePage performSearch(String searchTerm) {
        driver.findElement(By.id("lst-ib")).sendKeys(searchTerm + Keys.ENTER);
        return this;
    }

    public List checkSearchResultsURLContains(String contains) {
        final List<WebElement> elements = driver.findElements(By.tagName("cite"));
        return elements
                .stream()
                .filter(line -> line.getText().contains(contains))
                .collect(Collectors.toList());
    }

    public List checkSearchResultsDescriptionContains(String contains) {
        final List<WebElement> elements = driver.findElements(By.className("st"));
        return elements
                .stream()
                .filter(line -> line.getText().contains(contains))
                .collect(Collectors.toList());
    }

    public GooglePage searchOnPageLoad(String searchTerm) {
        driver.switchTo().activeElement().sendKeys(searchTerm + Keys.RETURN);
        return this;
    }

    public String getSearchedTerm() {
        waitForElement(By.id("resultStats"));
        return driver.getTitle().replace(" - Google Search", "");
    }

}