package pageobjects;

import infrastructure.TestUtils;
import org.openqa.selenium.JavascriptExecutor;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.with;
import static org.junit.Assert.fail;

public class AbstractPage extends TestUtils {

    protected String url;
    protected String expectedPageTitle;

    public AbstractPage() {
        buildPage();
        goToPage();
        waitForPage();
        validatePage();
    }

    protected void buildPage() {
    }

    protected void validatePage() {
        if (!checkPageTitle()) {
            takeScreenshot("Expected page to be " + expectedPageTitle + " ");
            fail("Page title is not correct. Expecting \""
                    + expectedPageTitle
                    + "\" actual \""
                    + driver.getTitle()
                    + "\"");
        }
        takeScreenshot("Page " + expectedPageTitle + " ");
    }

    private void goToPage() {
        driver.get(url);
    }

    private void waitForPage() {
        try {
            with()
                    .pollInterval(1, SECONDS)
                    .await()
                    .atMost(15, SECONDS)
                    .until(() -> checkForDocumentReadyState() == true);
        } catch (IllegalStateException timeout) {
            fail("Page \"" + expectedPageTitle + "\" took longer 15 seconds to render.");
        }
    }

    private Boolean checkForDocumentReadyState() {
        return ((JavascriptExecutor) driver)
                .executeScript("return document.readyState")
                .toString()
                .equals("complete");
    }

    private boolean checkPageTitle() {
        return driver.getTitle().matches(expectedPageTitle);
    }

}
