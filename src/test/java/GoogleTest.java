import infrastructure.PageNavigator;
import org.junit.Test;
import pageobjects.GooglePage;

import static junit.framework.TestCase.assertEquals;

public class GoogleTest {

    private PageNavigator pageNavigator = new PageNavigator();

    @Test
    public void shouldFocusOnSearchBoxOnPageLoad() {
        String searchTerm = "A new search term";
        assertEquals(searchTerm, pageNavigator.goToGoogle()
                .searchOnPageLoad(searchTerm).getSearchedTerm());
    }

    @Test
    public void shouldExcludeKeyWords() {
        assertEquals(0, pageNavigator.goToGoogle()
                .performSearch("cucumber -cucumber.io")
                .checkSearchResultsURLContains("cucumber.io").size());
    }

    @Test
    public void shouldSearchOnSpecificSite() {
        GooglePage googlePage = pageNavigator.goToGoogle();
        googlePage.performSearch("site:bbc.co.uk england");
        assertEquals(9, googlePage.checkSearchResultsURLContains("bbc.co.uk").size());
        assertEquals(9, googlePage.checkSearchResultsDescriptionContains("England").size());
    }

}